from flask import session, render_template, current_app

from info import create_app, db,models
from flask_script import Manager
from flask_migrate import MigrateCommand,Migrate

app = create_app()
manager = Manager(app)
Migrate(app,db)
manager.add_command("db",MigrateCommand)


# @app.route('/')
# def index():
#     # 如果是使用 rend_template 渲染页面，把页面放到 templates文件夹下，按照此方法渲染
#     return render_template('index.html') # 此路径默认有前缀' /templates/ '

@app.route('/')  # 此路径保留，测试使用
def test():
    # 如果是使用 current_app.send_static_file 渲染页面，把页面放到 static文件夹下，按照此方法渲染
    return current_app.send_static_file('html/index.html') # 此路径默认有前缀' /static/ '

if __name__=="__main__":
    print(app.url_map)
    manager.run()