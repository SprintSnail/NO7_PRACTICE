from datetime import timedelta
from logging import ERROR

from redis import StrictRedis

# 定义开发环境配置参数
class DevelopConf(object):
    # 调试模式和host port
    HOST = '127.0.0.1'
    PORT = 6379
    DEBUG = True
    # db 数据库配置
    SQLALCHEMY_DATABASE_URI = "mysql+pymysql://root:mysql@localhost:3306/db_ihome"

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    # 秘钥配置
    SECRET_KEY = 'kdalafjld'
    # Session配置
    SESSION_TYPE = 'redis'
    SESSION_REDIS = StrictRedis(host= HOST, port = PORT)
    SESSION_USE_SIGNER = True
    PERMANENT_SESSION_LIFETIME = timedelta(days=1)
    # 日志级别配置为 DEBUG
    LOG_LEVEL = DEBUG

# 定义测试环境配置参数
class TestConf(DevelopConf):
    pass

# 定义上线环境
class ProductCong(DevelopConf):
    DEBUG = ERROR

# 定义配置参数的字典
conf_dict = {'develop':DevelopConf,
             'test':TestConf,
             'product':DevelopConf
             }
