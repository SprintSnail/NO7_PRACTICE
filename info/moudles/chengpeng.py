
from datetime import datetime

from flask import current_app
from flask import g
from functools import wraps

import random

# from celery import current_app
from flask import json, jsonify
from flask import make_response
from flask import request
import re
from flask import session

from info import my_redis as redis_store, db
from info.constants import IMAGE_CODE_REDIS_EXPIRES, SMS_CODE_REDIS_EXPIRES
from info.models import User
from info.response_code import RET
from info.utils.captcha.captcha import captcha

from info.moudles import ihome_blue as passport_blue
# from info.libs.yuntongxun.sms import CCP



# 定义登陆装饰器，封装用户的登陆数据
def user_login_data(view_func):
    @wraps(view_func)
    def wrapper(*args,**kwargs):
        # 1.从session中取出用户的user_id
        # from flask import session
        user_id = session.get("user_id")

        # 2.通过user_id取出对象
        user = None
        if user_id:
            try:
                from info.models import User
                user = User.query.get(user_id)
            except Exception as e:
                current_app.logger.error(e)


        # 将用户对象封装到g对象
        g.user = user

        return view_func(*args,**kwargs)
    return wrapper



# 退出
@passport_blue.route('/logout', methods=['POST'])
def logout():
    """
    1. 清除session
    2. 返回响应
    :return:
    """
    # 1.清除session
    session.pop("user_id",None)

    # 2.返回响应
    return jsonify(errno=RET.OK,errmsg="退出成功")

@passport_blue.route("/login.html",methods=["GET"])
def login_news():
    return current_app.send_static_file('html/login.html')
# 登陆
@passport_blue.route("/session",methods=["POST"])
def login():
    """
    1. 获取参数
    2. 校验参数,为空校验
    3. 通过用户手机号,到数据库查询用户对象
    4. 判断用户是否存在
    5. 校验密码是否正确
    6. 将用户的登陆信息保存在session中
    7. 返回响应
    :return:
    """
    # 1.获取参数

    mobile = request.json.get("mobile")
    password = request.json.get("password")
    # 2.校验参数, 为空校验
    if not all([mobile,password]):
        return jsonify(errno=RET.NODATA,errmsg="参数不足")
    # 3.通过用户手机号, 到数据库查询用户对象
    try:
        user = User.query.filter(User.mobile == mobile).first()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR,errmsg="获取失败")

    # 4.判断用户是否存在
    if not user:
        return jsonify(jsonify(errno=RET.USERERR,errmsg="该用户不存在"))

    # 5.校验密码是否正确
    if not user.check_passowrd(password):
        return jsonify(errno=RET.DATAERR,errmsg="密码错误")

    # 6.将用户的登陆信息保存在session中
    session["user_id"] = user.id

    user.last_login = datetime.now()

    # 7.返回响应
    return jsonify(errno=RET.OK,errmsg="登陆成功")

# 注册
@passport_blue.route('/register.html',methods=['GET'])
def register():
    return current_app.send_static_file('html/register.html')


@passport_blue.route('/user',methods=['POST'])
def register_new():
    """
       1. 获取参数
      2. 校验参数,为空校验
      3. 手机号作为key取出redis中的短信验证码
      4. 判断短信验证码是否过期
      5. 判断短信验证码是否正确
      6. 删除短信验证码
      7. 创建用户对象
      8. 设置用户对象属性
      9. 保存用户到数据库
      10. 返回响应

    :return:
    """

    mobile = request.json.get("mobile")
    sms_code = request.json.get("phonecode")
    password = request.json.get("password")

    # 2.校验参数, 为空校验
    if not all([mobile,sms_code,password]):
        return jsonify(errno=RET.PARAMERR,errmsg="不能有空值")

    # 3.手机号作为key取出redis中的短信验证码
    try:
        redis_sms_code = redis_store.get("sms_code:%s"% mobile)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR,errmsg="短信验证码取出失败")

    # 4.判断短信验证码是否过期
    if not redis_sms_code :
        return jsonify(errno=RET.NODATA,errmsg="短信验证码已过期")

    # 5.判断短信验证码是否正确
    if not sms_code:
        return jsonify(errno=RET.DATAERR,errmsg="短信验证码错误")

    # 6.删除短信验证码
    try:
        redis_store.delete("sms_code:%s"% mobile)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR,errmsg="删除短信验证码错误")

    # 7.创建用户对象
    user = User()

    # 8.设置用户对象属性
    user.name = mobile
    user.password = password
    user.mobile = mobile


    # 9.保存用户到数据库
    try:
        db.session.add(user)
        db.session.commit()

    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR,errmsg="创建用户失败")
    # 10.返回响应
    return jsonify(errno=RET.OK,errmsg="注册成功")



# 获取图片验证码
@passport_blue.route("/imagecode")
def image_code():

    # 获取前段传递的参数
    cur_id = request.args.get("cur")
    per_id = request.args.get("pre")

    # 调用generaye_captcha获取图片验证码编号，验证码值，图片（二进制）
    name,text,image_data = captcha.generate_captcha()
    print(text)

    # 讲图片验证码的值包船redis(key,value,有效期)


     # 判断图片去重
    try:
        redis_store.set("image_code:%s"% cur_id, text, IMAGE_CODE_REDIS_EXPIRES)
        if per_id:
         redis_store.delete("image_code:%s"%per_id)
    except Exception as e:
        current_app.logger.error(e)
        return "操作失败"

    # 返回图片
    response = make_response(image_data)
    response.headers["Content-Type"] = "image/ipg"
    return response


# 获取短信验证码
@passport_blue.route('/smscode', methods=['POST'])
def smscode():
    """
    1. 获取参数
    2. 参数的为空校验
    3. 校验手机的格式
    4. 通过图片验证码编号获取,图片验证码
    5. 判断图片验证码是否过期
    6. 判断图片验证码是否正确
    7. 删除redis中的图片验证码
    8. 生成一个随机的短信验证码, 调用ccp发送短信,判断是否发送成功
    9. 将短信保存到redis中
    10. 返回响应
    :return:
    """
    # 1. 获取参数
    mobile = request.json.get("mobile")
    image_code = request.json.get("image_code")
    image_code_id = request.json.get("image_code_id")

    # 2. 参数的为空校验
    if not all([mobile,image_code,image_code_id]):
        return jsonify(errno=RET.PARAMERR,errmsg="参数不全")

    # 3. 校验手机的格式
    if not re.match("1[3-9]\d{9}",mobile):
        return jsonify(errno=RET.DATAERR,errmsg="手机号的格式错误")

    # 4. 通过图片验证码编号获取,图片验证码
    try:
        redis_image_code = redis_store.get("image_code:%s"% image_code_id)
        redis_image_code=redis_image_code.decode()
        print(redis_image_code)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR,errmsg="操作redis失败")

    # 5. 判断图片验证码是否过期
    if not redis_image_code:
        return jsonify(errno=RET.NODATA,errmsg="图片验证码已经过期")

    # 6. 判断图片验证码是否正确
    if image_code.upper() != redis_image_code.upper():
        return jsonify(errno=RET.DATAERR,errmsg="图片验证码填写错误")

    # 7. 删除redis中的图片验证码
    try:
        redis_store.delete("image_code:%s"%image_code_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR,errmsg="删除redis图片验证码失败")

    # 8. 生成一个随机的短信验证码, 调用ccp发送短信,判断是否发送成功
    sms_code = "%06d"%random.randint(0,999999)
    # print(sms_code)
    current_app.logger.debug(sms_code)
    # ccp = CCP()

    # 参数1mobile: 要给哪个手机号发送短信    参数2: ["验证码",有效期]  参数3: 模板编号默认就是1
    # 【云通讯】您使用的是云通讯短信模板，您的验证码是{1}，请于{2}分钟内正确输入
    # result = ccp.send_template_sms(mobile,[sms_code,constants.SMS_CODE_REDIS_EXPIRES/60],1)

    # if result == -1:
    #     return jsonify(errno=RET.DATAERR,errmsg="短信发送失败")

    # 9. 将短信保存到redis中
    try:
        redis_store.set("sms_code:%s" % mobile, sms_code,SMS_CODE_REDIS_EXPIRES)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR,errmsg="图片验证码保存到redis失败")

    # 10. 返回响应
    return jsonify(errno=RET.OK,errmsg=sms_code)

