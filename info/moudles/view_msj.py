from flask import current_app
from flask import g, jsonify
from flask import request

from info import constants
from info import db
from info.models import User, Area, House
from info.response_code import RET
from info.utils.common_method import is_login
from info.utils.qiniu.qiniu import image_storage
from . import ihome_blue,index_blue
'''
url:/api/v1.0/search.html?aid=2&aname=西城区&sd=2019-01-24&ed=2019-01-27
请求方式:GET
请求参数:aid,sd,ed,sk,p
返回数据:响应数据
'''
@index_blue.route('/search.html')
def show_search():
    return current_app.send_static_file('html/search.html')
@ihome_blue.route('/houses')
def houses():
    '''
    1 获取参数
    2 为空校验
    3 根据参数查询数据
    4 拼接数据
    5 返回响应
    :return:
    '''
    # 1 获取参数
    aid = request.args.get('aid')
    sk = request.args.get('sk')
    p = request.args.get('p')
    sd = request.args.get('sd')
    ed = request.args.get('ed')
    # 2 为空校验
    if not all([aid,sd,ed,sk,p]):
        return jsonify(errno = RET.PARAMERR , errmsg = '参数不全' )
    # 3 根据参数查询数据
    try:
        houses = House.query.filter(House.area_id == aid ).all()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR,errmsg="操作redis失败")
    # 4 拼接数据
    houses_list = []
    for house in houses:
        houses_list.append(house.to_basic_dict())
    data={
        'houses':houses_list
    }
    # 5 返回响应
    return jsonify(errno=RET.OK, errmsg='数据查询成功', data=data)
'''
url:/api/v1.0/houses/index
请求方式:GET
请求参数:无
返回数据:响应数据
'''
@ihome_blue.route('/houses/index')
def houses_index():
    '''
    1 查询房源数据
    2 拼接数据
    3 返回响应
    :return:
    '''
    # 1 查询房源数据
    try:
        houses = House.query.order_by(House.create_time.desc()).limit(3).all()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR,errmsg="操作redis失败")
    # 2 拼接数据
    houses_list = []
    for house in houses:
        houses_list.append(house.to_basic_dict())
    # 3 返回响应
    return jsonify(errno = RET.OK , errmsg = '数据查询成功',data = houses_list)
'''
url:/api/v1.0/areas
请求方式:GET
请求参数:无
返回数据:响应数据
'''
@ihome_blue.route('/areas')
def areas():
    '''
    1 查询城区数据
    2 拼接数据
    3 返回响应
    :return:
    '''
    # 1 查询城区数据
    try:
        areas = Area.query.all()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR,errmsg="操作redis失败")
    if not areas:return jsonify(errno = RET.NODATA , errmsg = '城区数据未查询' )
    # 2 拼接数据
    areas_list = []
    for aera in areas:
        areas_list.append(aera.to_dict())
    # 2 返回响应
    return jsonify(errno = RET.OK , errmsg = '数据查询成功',data = areas_list )
'''
url:/favicon.ico
请求方式:GET
请求参数:无
返回数据:静态文件index.html
'''
@ihome_blue.route('/favicon.ico')
def show_log():
    '''
    返回静态页面
    :return:
    '''
    return current_app.send_static_file('favicon.ico')
'''
url:/api/v1.0/
请求方式:GET
请求参数:无
返回数据:静态文件index.html
'''
@ihome_blue.route('/')
def show_index():
    '''
    返回静态页面
    :return:
    '''
    return current_app.send_static_file('html/index.html')
'''
url:/api/v1.0/user/auth
请求方式:GET,POST
请求参数:real_name,id_card
返回数据:data(用户信息)
'''
@ihome_blue.route('/user/auth', methods=['GET', 'POST'])
def user_auth():
    '''
    1 判断用户是否登入
    2 判断请求类型
    3 获取参数
    4 为空校验
    5 根据获取参数修改用户属性
    6 返回响应
    :return:
    '''
    user = User.query.get(7)
    # 1 判断用户是否登入
    if not user: return jsonify(errno=RET.NODATA, errmsg='用户未登入')
    # 2 判断请求类型
    if request.method == 'POST':
        # 3 获取参数
        real_name  = request.form.get('real_name')
        id_card  = request.form.get('id_card')
        # 4 为空校验
        if not all([real_name,id_card]):return jsonify(errno = RET.PARAMERR , errmsg = '参数不全' )
        # 5 根据获取参数修改用户属性
        user.real_name = real_name
        user.id_card = id_card

        db.session.commit()
    else:
        # 获取用户的数据
        return jsonify(errno = RET.OK , errmsg = '查询成功' ,data=user.to_auth_info())
    # 6 返回响应
    return jsonify(errno = RET.OK , errmsg = '认证成功' )
'''
url:/api/v1.0/user/name
请求方式:POST
请求参数:name
返回数据:data(用户信息)
'''
@ihome_blue.route('/user/name', methods=['POST'])
def user_name():
    '''
    1 判断用户是否登入
    2 获取参数
    3 为空校验
    4 根据获取参数修改用户属性
    5 返回响应
    :return:
    '''
    user = User.query.get(7)
    # 1 判断用户是否登入
    if not user: return jsonify(errno=RET.NODATA, errmsg='用户未登入')
    # 2 获取参数
    name = request.form.get('name')
    # 3 为空校验
    if not name:return jsonify(errno = RET.NODATA , errmsg = '名字未输入' )
    # 4 根据获取参数修改用户属性
    user.name = name
    # 5 返回响应
    return jsonify(errno = RET.OK , errmsg = '用户名修改成功' )
'''
url:/api/v1.0/user/avatar
请求方式:POST
请求参数:avatar
返回数据:data(用户信息)
'''
@ihome_blue.route('/user/avatar', methods=['POST'])
def avatar():
    '''
    1 判断用户是否登入
    2 获取参数
    3 为空校验
    4 根据获取参数修改用户属性
    5 返回响应
    :return:
    '''
    user = User.query.get(7)
    # 1 判断用户是否登入
    if not user: return jsonify(errno=RET.NODATA, errmsg='用户未登入')
    # 2 获取参数
    '''参数取法可能有误'''
    avatar = request.files.get('avatar')
    # 3 为空校验
    if not avatar:return jsonify(errno = RET.NODATA , errmsg = '图片地址不存在' )
    # 4. 上传图片,判断是否上传成功
    try:
        # 读取图片为二进制数据,上传
        image_name = image_storage(avatar.read())
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.THIRDERR, errmsg="七牛云异常")

    if not image_name:
        return jsonify(errno=RET.NODATA, errmsg="图片上传失败")
    # 5 根据获取参数修改用户属性
    user.avatar_url = constants.QINIU_DOMIN_PREFIX + image_name

    # 6 返回响应
    return jsonify(errno = RET.OK , errmsg = '文件上传成功' ,data=user.to_dict())
'''
url:/api/v1.0/user
请求方式:GET
请求参数:无
返回数据:data(用户信息)
'''
@ihome_blue.route('/user')
# @is_login
def user():
    '''
    1 判断用户是否登入
    2 将用户对象转换成用户字典
    3 返回数据
    :return:
    '''
    user = User.query.get(7)
    # 1 判断用户是否登入
    # if not g.user :return jsonify(errno = RET.NODATA , errmsg = '用户未登入' )
    if not user :return jsonify(errno = RET.NODATA , errmsg = '用户未登入' )
    # 2 将用户对象转换成用户字典
    data = user.to_dict()
    # 3 返回数据
    return jsonify(errno = RET.OK , errmsg = '登入成功',data=data )
