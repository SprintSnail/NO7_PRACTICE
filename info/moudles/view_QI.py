from flask import current_app, session, g, jsonify, request
from flask_wtf.csrf import generate_csrf
from flask import Blueprint

from info import db
from info.models import Order, House, Area, Facility
from info.utils.common_method import is_login
from . import ihome_blue,new_blue

@ihome_blue.route('/orders.html')
@is_login
def ihome_blue_orders_func():
    orders_list = []
    if g.user:
        orders = Order.query.filter(Order.user_id==g.user.id).all()
        if orders:
            for order in orders:
                orders_list.append(order.to_dict())
    else:
        orders = None
    # return current_app.send_static_file('html/orders.html',orders=orders_list)
    return current_app.send_static_file('html/orders.html')


# --------个人中心，我的订单,展示 -----
@ihome_blue.route('/orders')
@is_login
def ihome_blue_show_order_func():
    role = request.args.get('role')
    data = {}
    if role =='custom':
        if role in ['custom','landlord'] and g.user:
            orders = Order.query.filter(Order.user_id==g.user.id).all()
            if not orders:return jsonify(errno=3,errmsg = 'wrong')
            order_list = []
            for order in orders:
                order_list.append(order.to_dict())
            data['orders']= order_list
            return jsonify(errno = '0',errmsg = 'ok',data = data )
        else:
            return jsonify(errno=3, errmsg='no user or wrong action')
        # if role
        # return jsonify(errno=0,errmsg='test')

    else :
        pass

# -----提交评论-------
@ihome_blue.route('/orders/comment',methods=['PUT'])
@is_login
def ihome_blue_order_comment_func():
    if not g.user:
        return jsonify(errno='4101',errmsg='wrong')
    order_id= request.json.get('order_id')
    comment= request.json.get('comment')
    if not all([comment,order_id]):
        return jsonify(errno = 3,errmsg='lose parameter')
    try:
        current_order = Order.query.filter(Order.id ==order_id).first()
        current_order.comment = comment
        current_order.status = 'COMPLETE'
    except:
        return jsonify(errno=3, errmsg='lose parameter')
    return jsonify(errno = '0',errmsg = 'ok')

@ihome_blue.after_request
def hahahaha(res):
    value = generate_csrf()
    res.set_cookie('csrf_token',value)
    return res

# ----页首返回上一页功能-----
# @new_blue.route('/my.html')
# def new_blue_my_func():
#     return current_app.send_static_file('html/my.html')

# -----我的房源展示------
@ihome_blue.route('/myhouse')
def ihome_blue_my_house_show_func():
    return current_app.send_static_file('html/myhouse.html')


# -----判断是否实名-----
@ihome_blue.route('/user/auth')
@is_login
def ihome_blue_user_real_func():
    if g.user and g.user.id_card:
        data={}
        data['real_name'] = g.user.real_name
        data['id_card'] = g.user.id_card
        return jsonify(errno ='0',errmsg='OK',data=data)
    else:
        return jsonify(errno='4101',errmsg='pleas login')

# ----获取实名客户的房源信息------
@ihome_blue.route('/user/houses')
@is_login
def ihome_blue_my_house_func():
    if not g.user:
        return jsonify(errno = "4101",errmsg='wrong')
    house_list = []
    try:
        houses = House.query.filter(House.user_id==g.user.id).all()
        if houses:
            for house in houses:
                house_list.append(house.to_basic_dict())
    except:
        pass
    return jsonify(errno = '0',errmsg='OK',data = house_list)

# ---------04.1发布房源----------------
@new_blue.route('/newhouse.html')
@is_login
def ihome_blue_new_house_func():
    if not g.user:
        return 'no user please log in'
    return current_app.send_static_file('html/newhouse.html')
# -------04.2发布房源页面，获取区域
@ihome_blue.route('/areas')
@is_login
def ihome_blue_get_areas_func():
    if not g.user:
        return jsonify(errno = 3,errmsg = 'please login first')
    area_list = []
    try:
        get_areas = Area.query.all()
        if not get_areas:return jsonify(errno = '0',errmsg='OK',data = area_list)
        for get_area in get_areas:
            area_list.append(get_area.to_dict())
    except:
        return jsonify(errno = 3, errmsg = 'connect timeout')
    return jsonify(errno='0', errmsg='OK', data=area_list)
# ----04.3获取发布房屋的详细数据---------
@ihome_blue.route('/houses',methods=['POST'])
@is_login
def ihome_blue_get_detail_func():
    # | title | true | str | 标题 |
    # | price | true | str | 价格 |
    # | area_id | true | int | 城区id |
    # | address | true | str | 房屋地址 |
    # | room_count | true | int | 房间数目 |
    # | acreage | true | int | 房屋面积 |
    # | unit | true | str | 房屋单元，如：几室几厅 |
    # | capacity | true | int | 房屋容纳的人数 |
    # | beds | true | str | 房屋床铺的配置 |
    # | deposit | true | str | 房屋押金 |
    # | min_days | true | int | 最少入住天数 |
    # | max_days | true | int | 最大入住天数，0
    # 表示不限制 |
    # | facility | true | array | 用户选择的设施信息id列表，如：[7, 8] |
    if not g.user:return jsonify(errno = 3,errmsg = 'no user')
    title=request.json.get('title')
    price=request.json.get('price')
    area_id=request.json.get('area_id')
    address=request.json.get('address')
    room_count=request.json.get('room_count')
    acreage=request.json.get('acreage')
    unit=request.json.get('unit')
    capacity=request.json.get('capacity')
    beds=request.json.get('beds')
    deposit=request.json.get('deposit')
    min_days=request.json.get('min_days')
    max_days=request.json.get('max_days')
    facility=request.json.get('facility')
    if not all([facility,max_days,min_days,deposit,beds,capacity,unit,acreage,room_count,address,area_id,price,title]):
        return jsonify(errno = 3,errmsg = 'lose parameters')
    try:
        price=int(price);area_id=int(area_id);price=int(price);area_id=int(area_id);
        room_count = int(room_count);acreage = int(acreage);capacity = int(capacity);deposit = int(deposit);
        min_days = int(min_days);max_days = int(max_days)
    except:
        return jsonify(errno = 3,errmsg = 'unexpeted parameters')
    try:
        facility_list = Facility.query.all()
        if facility_list:
            facility_lists = [i.id for i in facility_list]
        for ik in facility:
            ik = int(ik)
            if ik not in facility_lists:return jsonify(errno = 3,errmsg = 'wrong parameters')
        new_house = House()
        new_house.user_id = g.user.id
        new_house.area_id = area_id
        new_house.title = title
        new_house.price = price
        new_house.address = address
        new_house.room_count = room_count
        new_house.acreage = acreage
        new_house.unit = unit
        new_house.capacity = capacity
        new_house.beds = beds
        new_house.deposit = deposit
        new_house.min_days = min_days
        new_house.max_days = max_days
        new_house_facility_list=[]
        for num in facility:
            num = int(num)
            new_house_facility_list.append(Facility.query.get(num))
        new_house.facilities = new_house_facility_list
        db.session.add(new_house)
        db.session.commit()
        print(new_house.id)
    except:
        return jsonify(errno = 3,errmsg = 'connect timeout')
    return jsonify(errno = "0",errmsg = 'OK',data = {'house_id':new_house.id})

# ----04.4发布房源上传图片--------
@ihome_blue.route('/houses/<int:num_house>/images',methods=['POST'])
@is_login
def ihome_blue_put_house_image_func(num_house):
    if not g.user:return jsonify(errno = 3, errmsg = 'please login first')
    image_file = request.files.get('house_image')
    if not image_file:return jsonify(errno = 3, errmsg = 'empty image')
    from info.utils.qiniu.qiniu import image_storage
    image_url = image_storage(image_file.read())
    try:
        current_house = House.query.get(num_house)
        current_house.index_image_url = image_url
    except:
        return jsonify(errno = 3,errmsg = 'connect timeout')
    return jsonify(errno = '0',errmsg='OK',data = {'url':'http://pky92f7rj.bkt.clouddn.com/'+image_url})


