from functools import wraps
from flask import session, g

# 定义装饰函数，判断用户是否
def is_login(func):
    @wraps(func)
    def set_func(*args,**kwargs):
        from ..models import User
        g.user = ''
        # 如果 用户已经登陆了，获取成功的话，g.user 的值是用户对象
        try:
            user_id = session.get('user_id')
            user = User.query.filter(User.id==user_id).first() if user_id else ''
            if user:
                g.user = user
        except:
            return '连接超时，请重试'
        return func(*args,**kwargs)
    return set_func