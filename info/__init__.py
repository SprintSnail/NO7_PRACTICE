from logging.handlers import RotatingFileHandler
import logging
from flask import Flask
from flask.ext.wtf.csrf import generate_csrf
from flask_session import Session
from flask_wtf.csrf import CSRFProtect,generate_csrf

from config import conf_dict
from flask_sqlalchemy import SQLAlchemy
from redis import StrictRedis

db = SQLAlchemy()
my_redis=None
# 定义 Create_app()函数
def create_app():
    # 获取配置对象
    use_conf = conf_dict['develop']
    LEVEL_NAME = use_conf.LOG_LEVEL
    app = Flask(__name__)
    app.config.from_object(use_conf)

    # 配置DB 数据库
    db.init_app(app)

    # 配置 redis
    global my_redis
    my_redis = StrictRedis(host='127.0.0.1',port =6379)

    # 配置Session 和 CSRF
    Session(app)
    CSRFProtect(app)
    
    # 生成日志文件
    log_file(LEVEL_NAME)
    
    # 注册蓝图
    from info.moudles import ihome_blue,index_blue
    app.register_blueprint(ihome_blue)
    app.register_blueprint(index_blue)


    # 使用请求钩子拦截所有的请求,通过的在cookie中设置csrf_token
    @app.after_request
    def after_request(resp):
        # 调用系统方法,获取csrf_token
        csrf_token = generate_csrf()

        # 将csrf_token设置到cookie中
        resp.set_cookie("csrf_token", csrf_token)

        # 返回响应
        return resp

    return app


# 定义日志函数
def log_file(LEVEL_NAME):
    # 设置日志的记录等级,常见的有四种,大小关系如下: DEBUG < INFO < WARNING < ERROR
    logging.basicConfig(level=LEVEL_NAME)  # 调试debug级,一旦设置级别那么大于等于该级别的信息全部都会输出
    # 创建日志记录器，指明日志保存的路径、每个日志文件的最大大小、保存的日志文件个数上限
    file_log_handler = RotatingFileHandler("logs/log", maxBytes=1024 * 1024 * 100, backupCount=10)
    # 创建日志记录的格式 日志等级 输入日志信息的文件名 行数 日志信息
    formatter = logging.Formatter('%(levelname)s %(filename)s:%(lineno)d %(message)s')
    # 为刚创建的日志记录器设置日志记录格式
    file_log_handler.setFormatter(formatter)
    # 为全局的日志工具对象（flask app使用的）添加日志记录器
    logging.getLogger().addHandler(file_log_handler)